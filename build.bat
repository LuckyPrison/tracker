@ECHO OFF

ECHO Building...

DEL build.log

CALL mvn clean package >> build.log

ECHO Deploying...

CALL java -jar copy.jar