# **LuckyPrison Repository** #

Welcome to the LuckyPrison Bitbucket code & bug repository! This is a project created and maintained by Packet (Adam Edwards).

## **Quick Links** ##
* [Our Website](https://luckyprison.com)
* [Issues](https://bitbucket.org/LuckyPrison/tracker/issues?status=new&status=open)
* [Create an issue](https://bitbucket.org/LuckyPrison/tracker/issues/new)
* [View some code](https://github.com/LuckyPrison)

## **Resources** ##
### GYAZO [(LINK)](https://gyazo.com) ###
Need to take a picture of your screen to submit as evidence? Use Gyazo. It's a free and one-click-use program that creates and uploads png or gif images straight from your computer.

### PASTEBIN [(LINK)](https://pastebin.com) ###
An easy way to upload permanent (or temporary) text-files, and access them on any device. It has a QR code scanner for your phone, so links are extra-easy to visit.